import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './GameHistory.css';
import { IGame } from '../../models/IGame';
import GameService from '../../services/GameService';
import Moment from 'react-moment';
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { IGameScore } from '../../models/IGameScore';
import TeamService from '../../services/TeamService';
import { ITeam } from '../../models/ITeam';
import { IGameDetails } from '../../models/IGameDetails';
import { LinkedinShareButton, LinkedinIcon } from 'react-share';
import AiBot from '../aiBot/aiBot';

export default class GameHistory extends React.Component<any, any> {

    private filterName: string = "gameType";

    constructor(props: any) {
        super(props);

        this.state = {
            state: [],
            games: [],
            gameType: '',
            teamNames: [],
            teamScores: [],
            fetching: false,
            filter: 'All'
        }
    }


    public componentWillMount() {
        this.setState({ filter: this.getParams(this.props.location, this.filterName) })
    }


    public componentDidMount() {
        GameService.getAllGames()
            .then((dbgames: IGame[]) => {
                this.setState({
                    games: dbgames
                })
            })
    }

    public getParams(location, filterName) {
        const searchParams = new URLSearchParams(location.search);
        return searchParams.get(filterName) || 'All';
    }

    public opened = (item: IGame) => {
        if (!this.state.fetching) {
            this.setState({ fetching: true, [item.id]: [], key: item.id });
            item.gameScore.map(this.renderGameScore)
        }
    }

    public renderGameScore = (item: IGameScore, index: number) => {
        TeamService.getTeamById(item.teamID)
            .then((team: ITeam) => {
                const data: IGameDetails = { team: team.name, isSingleUser: team.isSingleUser, score: item.score, place: item.place };
                const arr = this.state[this.state.key];
                arr.push(data);
                this.setState({ [this.state.key]: arr, fetching: false })
            })
    }

    public render() {

        const renderGamePlayers = (element: IGameDetails, i: number) => {
            let singleUserString = 'Team';
            if (element.isSingleUser) {
                singleUserString = 'User';
            }
            return (
                <tr>
                    <td style={{paddingRight: "20px"}}> {`${element.team} (${singleUserString})`} </td>
                    <td style={{paddingRight: "20px"}}> {element.score} </td>
                    <td style={{paddingRight: "20px"}}> {element.place} </td>
                </tr>
            )
        }

        const renderGameItem = (item: IGame, index: number) => {
            let name = 'Darts'
            if (item.gameType === 0) {
                name = 'Foosball'
            }
            if (this.state.filter === name || this.state.filter === 'All') {
                const img = new Image()
                img.src = item.image;
                return (
                    <ExpansionPanel CollapseProps={{ mountOnEnter: true }}
                        onClick={() => {
                            this.opened(item)
                        }}>

                        <ExpansionPanelSummary>
                            <Moment format="YYYY/MM/DD">{`${(item.date)}`}</Moment>
                            {` : ${name} : ${item.gameDescription}`}
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <table className="detailsTable" style={{textAlignLast: "center"}}>
                                <tr>
                                    <th> Team </th>
                                    <th> Score </th>
                                    <th> Place </th>
                                </tr>

                                {(this.state[item.id] !== undefined) ?
                                    this.state[item.id].map(renderGamePlayers) : null
                                }

                                {(item.image !== null) ? <img src={item.image} /> : ''}
                            </table>
                            <br />
                            <LinkedinShareButton title="proba" description="probaa" url={img.toString()}>
                                <LinkedinIcon />
                            </LinkedinShareButton>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                );
            }

        }

        const filters = [
            {
                label: 'Foosball',
            },
            {
                label: 'Darts',
            },
            {
                label: 'All',
            },
        ];

        const setParams = (key: string, value: string) => {
            const searchParams = new URLSearchParams();
            searchParams.set(key, value);
            return searchParams.toString();
        }

        const setQuery = (value: string) => {
            this.setState({ filter: value });
            this.props.history.push(`?${setParams(this.filterName, value)}`);
        }

        return (
            <span>
                <UserHome />
                <div className="lefttPadding">
                    <TextField
                        select
                        className="textField"
                        value={this.state.filter}
                        onChange={(event) => { setQuery(event.target.value) }}
                        helperText="Filter by type"
                        margin="normal"
                    >
                        {filters.map(option => (
                            <MenuItem key={option.label} value={option.label}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    <span>{this.state.games.map(renderGameItem)}</span>
                </div>
                <AiBot />    
            </span >
        );
    }
}