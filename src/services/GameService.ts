import axios, { AxiosResponse } from 'axios'
import { config } from '../utils/config';
import { IGame } from '../models/IGame';
import { IGameEntry } from '../models/IGameEntry';

export default class TeamService {

    public static getAllGames = (): Promise<IGame[]> => {
        return axios.get(`${config.apiUrl}/games`)
            .then((result: AxiosResponse) => result.data)
    }

    public static insertGame = (gameEntry: IGameEntry) => {
        return axios.post(`${config.apiUrl}/game`, gameEntry)
            .then((result: AxiosResponse) => result.data)
    }
}