import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './NewGame.css';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { IGameEntry } from '../../models/IGameEntry';
import { ITeam } from '../../models/ITeam';
import { IGameScore } from '../../models/IGameScore';
import { ITeamScore } from '../../models/ITeamScore';
import TeamService from '../../services/TeamService';
import GameService from '../../services/GameService';
import AiBot from '../aiBot/aiBot';

export default class NewGame extends React.Component<any, any> {

    private valid = true;

    constructor(props: any) {
        super(props);
        this.state = {
            marginPercent: '13.3%',
            gameDescription: '',
            gameDate: '',
            gameType: 0,
            currentDate: '',
            teams: [],
            databaseTeams: [],
            gameScores: [],
            uploadedFile: '',
            uploadedFileDisplay: ''
        }
    }

    public singleToDouble = (n: number) => {
        return n > 9 ? "" + n : "0" + n;
    }

    public componentWillMount() {
        const tempDate = new Date();
        const date = tempDate.getFullYear() + '-'
            + this.singleToDouble(tempDate.getMonth() + 1) + '-'
            + this.singleToDouble(tempDate.getDate()) + 'T'
            + this.singleToDouble(tempDate.getHours()) + ':'
            + this.singleToDouble(tempDate.getMinutes())
        this.setState({ currentDate: date, gameDate: tempDate })
    }

    public componentDidMount() {
        this.newTeamEntry()
        this.newTeamEntry()

        TeamService.getAllTeams()
            .then((dbteams: ITeam[]) => {
                this.setState({
                    teams: dbteams,
                    databaseTeams: dbteams.slice()
                })
            })

    }

    public newTeamEntry = () => {
        const array = this.state.gameScores;
        const data: IGameScore = { teamID: '', score: 0, place: 0 };
        array.push(data);
        this.setState({ gameScores: array });
    }

    public removeTeamEntry = () => {
        const array = this.state.gameScores;
        array.pop();
        this.setState({ gameScores: array });
    }

    public arrayElementSetState = (event, index: number, element: string) => {
        const items = this.state.gameScores;
        const itemm = items[index];
        switch (element) {
            case "teamID": {
                itemm.teamID = event.target.value;                
                break;
            }
            case "score": {
                itemm.score = event.target.value;
                break;
            }
            case "place": {
                itemm.place = event.target.value;
                break;
            }
        }

        items[index] = itemm;
        this.setState({ gameScores: items })
    }

    public validateGameScore = (item: IGameScore, index: number) => {
        if (item.teamID === '') {
            this.valid = false;
        }else{
            const value = item.teamID
            const array = this.state.gameScores
            for(let i = index + 1; i < this.state.gameScores.length; i++){
                if(value === array[i].teamID){
                    this.valid = false;
                }
            }
        }
    }

    public validate = () => {
        this.valid = true;
        this.state.gameScores.map(this.validateGameScore);
        if (this.valid) {
            const gameEntry: IGameEntry = ({
                gameDescription: this.state.gameDescription,
                date: this.state.gameDate,
                gameType: this.state.gameType,
                gameScore: this.state.gameScores,
                image: this.state.uploadedFile
            });
            console.log(gameEntry)
            GameService.insertGame(gameEntry)
                .then(() => {
                    this.state.gameScores.map(this.updateGameScore)
                    window.location.href = "http://localhost:3000/history"
                })
        } else {
            window.alert("Complete every team entry with different teams, and try again.")
        }

    }

    public updateGameScore = (item: string, index: number) => {
        let tempScore = 1;
        switch (this.state.gameScores[index].place) {
            case "1": {
                tempScore = 10;
                break;
            }
            case "2": {
                tempScore = 7;
                break;
            }
            case "3": {
                tempScore = 5;
                break;
            }
        }
        const teamScore: ITeamScore = { gameType: this.state.gameType, score: tempScore };
        TeamService.updateTeamScore(this.state.gameScores[index].teamID, teamScore)
    }

    public render() {

        const renderTeams = (item: ITeam, index: number) => {
            let singleUserString = 'Team';
            if (item.isSingleUser) {
                singleUserString = 'User';
            }
            return (
                <MenuItem key={item.id} value={item.id}>
                    {`${item.name} (${singleUserString})`}
                </MenuItem>
            )
        }


        const renderGameScoreEntry = (item: string, index: number) => {
            return (
                <div key={index} className="inline">
                    <p>{index + 1}. team</p>
                    <div className="teamTextField, teamTextField1">
                        <TextField
                            required
                            select
                            value={this.state.gameScores[index].teamID}
                            onChange={(event) => { this.arrayElementSetState(event, index, "teamID") }}
                            helperText="Please select a team or a user."
                            margin="normal"
                        >
                            {this.state.teams.map(renderTeams)}
                        </TextField>
                    </div>
                    <div className="teamTextField, teamTextField2">
                        <TextField
                            required

                            type="number"
                            value={this.state.gameScores[index].score}
                            onChange={(event) => { this.arrayElementSetState(event, index, "score") }}
                            helperText="Add score"
                            margin="normal"
                        />
                    </div>
                    <div className="teamTextField, teamTextField2">
                        <TextField
                            required

                            type="number"
                            value={this.state.gameScores[index].place}
                            onChange={(event) => { this.arrayElementSetState(event, index, "place") }}
                            helperText="Add rank"
                            margin="normal"
                        />
                    </div>
                </div>
            );
        }


        const gameTypes = [
            {
                value: '0',
                label: 'Foosball',
            },
            {
                value: '1',
                label: 'Darts',
            },
        ];

        return (
            <span className="form">
                <UserHome />
                <div style={{ marginLeft: this.state.marginPercent }}>
                    <form >
                        <TextField
                            required
                            className="textField"
                            margin='normal'
                            type="text"
                            name="gameDescription"
                            helperText="Game description"
                            value={this.state.gameDescription}
                            style={{ borderColor: this.state.gameDescription }}
                            onChange={(event) => this.setState({ gameDescription: event.target.value })}
                        />
                        <br />
                        <TextField
                            className="textField"
                            id="date"
                            type="datetime-local"
                            helperText="Please select the datetime of the game"
                            defaultValue={this.state.currentDate.toString()}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            InputProps={{ inputProps: { max: this.state.currentDate } }}
                            onChange={(event) => this.setState({ gameDate: event.target.value })}
                        />
                        <br />
                        <TextField
                            select
                            className="textField"
                            value={this.state.gameType}
                            onChange={(event) => this.setState({ gameType: event.target.value })}
                            helperText="Please select the type of the game"
                            margin="normal"
                        >
                            {gameTypes.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                        <br />
                        <div>
                            {this.state.gameScores.map(renderGameScoreEntry)}
                            <br />
                            <br />
                            <input
                                type="button"
                                className="teamButton"
                                value="Add team entry"
                                onClick={() => this.newTeamEntry()}
                            />

                            <input
                                type="button"
                                className="teamButton"
                                value="Remove team entry"
                                onClick={() => this.removeTeamEntry()}
                            />
                        </div>

                        <br />
                        <input
                            type="file"
                            onChange={(event: any) => {
                                const reader = new FileReader();

                                reader.readAsDataURL(event.target.files[0]);
                                reader.onload = () => {
                                    this.setState({ uploadedFile: reader.result })
                                };
                                this.setState({
                                    uploadedFileDisplay: URL.createObjectURL(event.target.files[0]),
                                });

                            }
                            }
                        />
                        <br />
                        <img src={this.state.uploadedFileDisplay} />
                        <br />
                        <input
                            type="button"
                            className="button"
                            value="Submit"
                            onClick={() => this.validate()}
                        />

                    </form>
                </div>
                <AiBot />    
            </span>

        );
    }
}