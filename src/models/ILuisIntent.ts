export interface ILuisIntent {
    intent: string;
    score: number;
}