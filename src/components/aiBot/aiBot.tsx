import * as React from 'react';
import './aiBot.css';
import SpeechRecognition from 'react-speech-recognition'
import LuisService from '../../services/LuisService';
import { ILuisData } from '../../models/ILuisData';
import { ILuisEntity } from '../../models/ILuisEntity';
import { Link } from 'react-router-dom';

const options = {
    autoStart: false

}

class AiBot extends React.Component<any, any> {

    private recording = false;

    constructor(props: any) {
        super(props);
        this.state = {
            luisData: '',
            gameType: '',
            users: [],
            newTo: '',
            view: ''
        }
    }

    public handleUsers = (entity: ILuisEntity) => {
        if (entity.type === "User") {
            const tempUsers = this.state.users;
            tempUsers.push(entity.entity);
            this.setState({ users: tempUsers });
        }

    }

    public extractLuisData = (luis: ILuisData) => {
        let to;
        switch (luis.topScoringIntent.intent) {
            case "Play": {
                luis.entities.map((entity: ILuisEntity) => { if (entity.type === "Game") { this.setState({ gameType: entity.entity }) } });
                luis.entities.map(this.handleUsers);
                switch (this.state.gameType) {
                    case "Darts": {
                        to = {
                            pathname: "/dartsStandard",
                            users: this.state.users,
                        };
                    }
                    case "foosball": {
                        to = {
                            pathname: "/foosball",
                            users: this.state.users,
                        };
                    }
                }
            }
            case "View": {
                luis.entities.map((entity: ILuisEntity) => { if (entity.type === "Page") { this.setState({ view: entity.entity }) } });
                const path = "/"+this.state.view
                to = {
                    pathname: path
                };
            }
            case "AddNewTeam": {
                luis.entities.map(this.handleUsers);                
                to = {
                    pathname: 'newTeam',
                    users: this.state.users,
                };
            }
        }
        if (to !== undefined) {
            this.setState({ newTo: to });
        }
    }


    public render() {

        const { transcript, resetTranscript, browserSupportsSpeechRecognition } = this.props

        const handleRecord = () => {
            if (!this.recording) {
                this.props.resetTranscript();
                resetTranscript
                this.recording = true;
                this.props.startListening();
            } else {
                this.recording = false;
                this.props.stopListening();
                console.log(transcript)
                if (transcript !== ''){
                    LuisService.send(transcript)
                    .then((luis: ILuisData) => {
                        console.log(luis);
                        this.setState({ luisData: luis });
                        this.extractLuisData(luis);
                    }) 
                }                               
            }

        }

        if (!browserSupportsSpeechRecognition) {
            return null
        }
        return (
            <div className="rightCorner">
                <img onClick={handleRecord} style={{ height: 200, width: 200 }} src={require('../../img/robot.png')} />
                <br />
                <Link to={this.state.newTo}>
                    <p>{transcript}</p>
                </Link>
            </div>

        );
    }
}

export default SpeechRecognition(options)(AiBot)