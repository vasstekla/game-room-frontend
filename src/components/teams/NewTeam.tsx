import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './NewTeam.css';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import UserService from '../../services/UserService';
import { IUser } from '../../models/IUser';
import Chip from '@material-ui/core/Chip';
import TeamService from '../../services/TeamService';
import { ITeamScore } from '../../models/ITeamScore';
import Avatar from '@material-ui/core/Avatar';
import AiBot from '../aiBot/aiBot';

export default class NewTeam extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            marginPercent: '13.3%',
            teamName: '',
            users: [],
            addedUsers: [],
            addedUser: '',
            selectedUserID: '',
            databaseUsers: [],
            scoreInitiate: []
        }
    }

    public predefinedUsers = (user: string, index: number) => {       
        const findValue = this.state.users.find((userItem: IUser) => userItem.username === user);
        console.log(findValue)
        if(findValue !== undefined){
            this.handleAddedUsers(findValue.id);
        }              
}

    public componentDidMount() {
        const array = this.state.scoreInitiate;
        const data0: ITeamScore = { gameType: 0, score: 0 };
        const data1: ITeamScore = { gameType: 1, score: 0 };
        array.push(data0);
        array.push(data1);
        this.setState({ scoreInitiate: array })

        UserService.getAllUsers()
            .then((dbusers: IUser[]) => {
                this.setState({
                    users: dbusers,
                    databaseUsers: dbusers.slice()
                })
                if(this.props.location.users !== undefined){
                    this.props.location.users.map(this.predefinedUsers)
                }
            })
    }

    public handleAddedUsers = (value: string) => {
        this.setState({ addedUser: value })
        const array = this.state.addedUsers;
        array.push(value);
        this.setState({ addedUsers: array });

        const arrayUsers = this.state.users;
        const indexUsers = arrayUsers.indexOf(arrayUsers.find((user: IUser) => user.id === value));
        arrayUsers.splice(indexUsers, 1)
        this.setState({ users: arrayUsers });
    }

    public deleteChip = (user: IUser) => {

        const array = this.state.addedUsers;
        const index = array.indexOf(array.find((item: string) => item === user.id));
        array.splice(index, 1)
        this.setState({ addedUsers: array });

        const arrayUsers = this.state.users;
        arrayUsers.push(user);
        this.setState({ users: arrayUsers });

    }

    public validate = () => {
        TeamService.insertTeam({
            name: this.state.teamName,
            players: this.state.addedUsers,
            scores: this.state.scoreInitiate,
            isSingleUser: false
        })
            .then(() => {
                window.alert('Team added')
            });
    }

    public render() {

        const renderUsers = (item: IUser, index: number) =>
            <MenuItem key={item.id} value={item.id}>
                {`${item.firstName} ${item.lastName} (${item.username})`}
            </MenuItem>

        const renderAddedUsers = (item: string, index: number) => {
            const findValue = this.state.databaseUsers.find((userItem: IUser) => userItem.id === item)
            let display = findValue.profilePicture;
            if (display === '') {
                display = require('../../img/avatar.png')
            }
            return (
                <Chip
                    avatar={<Avatar alt={findValue.username} src={display} />}
                    label={findValue.username}
                    onDelete={() => this.deleteChip(findValue)}
                />
            );
        }

        return (
            <span className="form">
                <UserHome />
                <div style={{ marginLeft: this.state.marginPercent }}>
                    <form >
                        <TextField
                            required
                            className="textField"
                            margin='normal'
                            type="text"
                            name="teamName"
                            placeholder="Team name"
                            value={this.state.teamName}
                            style={{ borderColor: this.state.teamName }}
                            onChange={(event) => this.setState({ teamName: event.target.value })}
                        />
                        <br />
                        <TextField
                            select
                            className="textField"
                            value={this.state.addedUser}
                            onChange={(event) => { this.handleAddedUsers(event.target.value) }}
                            helperText="Add member"
                            margin="normal"
                        >
                            {this.state.users.map(renderUsers)}
                        </TextField>
                        <br />
                        {this.state.addedUsers.map(renderAddedUsers)}
                        <br />
                        <input
                            type="button"
                            className="button"
                            value="Submit"
                            onClick={() => this.validate()}
                        />
                    </form>
                </div>
                <AiBot />    
            </span>
        );
    }
}