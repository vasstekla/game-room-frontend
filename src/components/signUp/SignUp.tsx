import * as React from 'react';
import './SignUp.css';
import UserService from '../../services/UserService'
import { IUser } from '../../models/IUser'
import { Link } from 'react-router-dom';

const nameErrorString: string = "The first and last names have to start with uppercase, and they can contain only letters."
const usernameErrorString: string = "The username can contain only letters, numbers and spaces."
const emailAdressErrorString: string = "The email adress has to be in email format, and also from the company Accesa."
const passwordErrorString: string = "The passwords has to be at least 6 characters long, and the two passwords have to match."
const databaseUsernameErrorString = "This username is already taken, please choose another one."
const databaseEmailErrorString = "This email adress already exists in out database."

export default class SignUp extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            username: '',
            emailAdress: '',
            password: '',
            passwordAgain: '',

            firstNameColor: '',
            lastNameColor: '',
            usernameColor: '',
            emailAdressColor: '',
            passwordColor: '',
            passwordAgainColor: '',

            nameError: '',
            usernameError: '',
            emailAdressError: '',
            passwordError: '',
            databaseError: '',

            users: []
        }
    }

    public render() {
        return (
            <span>
                <p id="signup" />
                <span className="signUp">
                    <h1> Sign Up </h1>
                    <h2> Please note, that only Accesa employee can sign up on this page. </h2>
                    <h3> Sorry for any inconvenience. </h3>
                    <form>
                        <input
                            type="text"
                            name="firstName"
                            placeholder="First name"
                            value={this.state.firstName}
                            style={{ borderColor: this.state.firstNameColor }}
                            onChange={(event) => this.setState({ firstName: event.target.value })} />

                        <input
                            type="text"
                            name="lastName"
                            placeholder="Last Name"
                            value={this.state.lastName}
                            style={{ borderColor: this.state.lastNameColor }}
                            onChange={(event) => this.setState({ lastName: event.target.value })} />
                        <p>{this.state.nameError}</p>
                        <br />
                        <input
                            type="text"
                            className="inputField"
                            name="username"
                            placeholder="username"
                            value={this.state.username}
                            style={{ borderColor: this.state.usernameColor }}
                            onChange={(event) => this.setState({ username: event.target.value })} />
                        <p>{this.state.usernameError}</p>
                        <br />
                        <input
                            type="text"
                            className="inputField"
                            name="emailAdress"
                            placeholder="Accesa Email adress"
                            value={this.state.emailAdress}
                            style={{ borderColor: this.state.emailAdressColor }}
                            onChange={(event) => this.setState({ emailAdress: event.target.value })} />
                        <p>{this.state.emailAdressError}</p>
                        <br />
                        <input
                            type="password"
                            name="password"
                            placeholder="Password"
                            value={this.state.password}
                            style={{ borderColor: this.state.passwordColor }}
                            onChange={(event) => this.setState({ password: event.target.value })} />
                        <input
                            type="password"
                            name="passwordAgain"
                            placeholder="Password again"
                            value={this.state.passwordAgain}
                            style={{ borderColor: this.state.passwordAgainColor }}
                            onChange={(event) => this.setState({ passwordAgain: event.target.value })} />
                        <p>{this.state.passwordError}</p>
                        <br />
                        <p>{this.state.databaseError}</p>
                        <br />
                        <input
                            type="button"
                            className="button"
                            value="Submit"
                            onClick={(event) => this.validate()} />
                    </form>

                </span>
            </span>

        );
    }

    private validate() {
        let valid = true
        let name = true
        if (!/^[A-Z][a-zA-Z ]+$/.test(this.state.firstName)) {
            this.setState({ firstNameColor: "red" })
            this.setState({ nameError: nameErrorString })
            name = false
            valid = false
        }
        else {
            this.setState({ firstNameColor: '' })
        }

        if (!/^[A-Z][a-zA-Z ]+$/.test(this.state.lastName)) {
            this.setState({ lastNameColor: "red" })
            this.setState({ nameError: nameErrorString })
            name = false
            valid = false
        }
        else {
            this.setState({ lastNameColor: '' })
        }

        if (name) {
            this.setState({ nameError: '' })
        }

        if (!/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(this.state.username)) {
            this.setState({ usernameColor: "red" })
            this.setState({ usernameError: usernameErrorString })
            valid = false
        }
        else {
            this.setState({ usernameColor: '' })
            this.setState({ usernameError: '' })
        }

        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(((accesa+\.)+[a-zA-Z]{2,}))$/.test(this.state.emailAdress)) {
            this.setState({ emailAdressColor: "red" })
            this.setState({ emailAdressError: emailAdressErrorString })
            valid = false
        }
        else {
            this.setState({ emailAdressColor: '' })
            this.setState({ emailAdressError: '' })
        }

        if (this.state.password !== this.state.passwordAgain || !/^.{6,}$/.test(this.state.password)) {
            this.setState({ passwordColor: "red" })
            this.setState({ passwordAgainColor: "red" })
            this.setState({ passwordError: passwordErrorString })
            valid = false
        }
        else {
            this.setState({ passwordColor: '' })
            this.setState({ passwordAgainColor: '' })
            this.setState({ passwordError: '' })
        }

        UserService.getAllUsers()
            .then((dbUsers: IUser[]) => {
                this.setState({ users: dbUsers })
                const emailfound = this.state.users.find((element: IUser) => {
                    return element.emailAdress === this.state.emailAdress;
                });

                const usernamefound = this.state.users.find((element: IUser) => {
                    return element.username === this.state.username;
                });

                if (!usernamefound) {
                    if (!emailfound) {
                        if (valid) {
                            UserService.insertUser({
                                firstName: this.state.firstName,
                                lastName: this.state.lastName,
                                username: this.state.username,
                                emailAdress: this.state.emailAdress,
                                password: this.state.password,
                                token: '',
                                profilePicture: '',
                            }).then(() => {
                                window.alert('Sign up succesfully')
                                this.setState({ databaseError: ''})
                                return(
                                    <Link to="/profile"/>
                                );
                                
                            });
                        }
                    } else {
                        this.setState({ emailAdressColor: "red" })
                        this.setState({ databaseError: databaseEmailErrorString })
                    }
                } else {
                    this.setState({ usernameColor: "red" })
                    this.setState({ databaseError: databaseUsernameErrorString })
                }
            })






    }
}