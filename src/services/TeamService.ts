import axios, { AxiosResponse } from 'axios'
import { config } from '../utils/config';
import { ITeam } from '../models/ITeam';
import { ITeamEntry } from '../models/ITeamEntry';
import { ITeamScore } from '../models/ITeamScore';

export default class TeamService {

    public static getAllTeams = (): Promise<ITeam[]> => {
        return axios.get(`${config.apiUrl}/team/getAll`)
            .then((result: AxiosResponse) => result.data)
    }

    public static getAllTeamsWithPagination = (pageSize: number, pageNumber: number ): Promise<ITeam[]> => {
        return axios.get(`${config.apiUrl}/team/getAllPagination?pageSize=${pageSize}&&pageNumber=${pageNumber}`)
            .then((result: AxiosResponse) => result.data)
    }

    public static getTeamById = (teamId: string): Promise<ITeam> => {
        return axios.get(`${config.apiUrl}/team?teamId=${teamId}`)
            .then((result: AxiosResponse) => result.data)
    }

    public static insertTeam = (team: ITeamEntry) => {
        return axios.post(`${config.apiUrl}/team`, team)
            .then((result: AxiosResponse) => result.data)
    }

    public static insertSingleUserTeam = (teamname: string, id: string) => {
        return axios.post(`${config.apiUrl}/team?teamname=${teamname}&memberid=${id}`)
            .then((result: AxiosResponse) => result.data)
    }

    public static updateTeamName = (teamName: string, name: string) => {
        return axios.put(`${config.apiUrl}/team?teamName=${teamName}&newName=${name}`)
            .then((result: AxiosResponse) => result.data)
    }

    public static updateTeamScore = (teamId: string, teamScore: ITeamScore) => {
        return axios.put(`${config.apiUrl}/team/${teamId}`, teamScore)
            .then((result: AxiosResponse) => result.data)
    }



}