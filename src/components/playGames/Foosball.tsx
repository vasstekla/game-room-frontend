import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './Foosball.css';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { ITeam } from '../../models/ITeam';
import Chip from '@material-ui/core/Chip';
import TeamService from '../../services/TeamService';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { IGameEntry } from '../../models/IGameEntry';
import { IGameScore } from '../../models/IGameScore';
import GameService from '../../services/GameService';
import { ITeamScore } from '../../models/ITeamScore';
import AiBot from '../aiBot/aiBot';

export default class Foosball extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            marginPercent: '13.3%',
            teamName: '',
            teams1: [],
            teams2: [],
            addedTeams1: [],
            addedTeams2: [],
            addedTeam1: '',
            addedTeam2: '',
            selectedTeamID: '',
            databaseTeams: [],
            teamScore1: 0,
            teamScore2: 0,
            playerSelect: true,
            description: '',
            gameEnded: false,
            uploadedFile: '',
            uploadedFileDisplay: '',
            gameScores: [],
        }
    }

    public predefinedUsers = (username: string, index: number) => {
        const findValue = this.state.teams2.find((userItem: ITeam) => userItem.name === username);
        console.log(findValue)
        if (findValue !== undefined) {
            this.handleAddedTeams2(findValue.id);
        }
    }

    public componentDidMount() {
        TeamService.getAllTeams()
            .then((dbteams: ITeam[]) => {
                this.setState({
                    teams1: dbteams,
                    teams2: dbteams,
                    databaseTeams: dbteams.slice()
                })
                if (this.props.location.users !== undefined) {
                    this.props.location.users.map(this.predefinedUsers)
                }
            })
    }

    public validate = () => {
        if (this.state.addedTeams1.length === 0 || this.state.addedTeams2.length === 0) {
            window.alert("Please select at least 1 player on each team")
        } else {
            this.setState({ playerSelect: false })
        }
    }

    public renderGameScores1 = (team: string, index: number) => {
        const array = this.state.gameScores;
        const data: IGameScore = { teamID: team, score: this.state.teamScore1, place: this.state.teamRank1 };
        array.push(data);
        this.setState({ gameScores: array });
    }

    public renderGameScores2 = (team: string, index: number) => {
        const array = this.state.gameScores;
        const data: IGameScore = { teamID: team, score: this.state.teamScore2, place: this.state.teamRank2 };
        array.push(data);
        this.setState({ gameScores: array });
    }

    public updateGameScore = (item: string, index: number) => {
        let tempScore = 1;
        switch (this.state.gameScores[index].place) {
            case 1: {
                tempScore = 10;
                break;
            }
            case 2: {
                tempScore = 7;
                break;
            }
            case 3: {
                tempScore = 5;
                break;
            }
        }
        const teamScore: ITeamScore = { gameType: 0, score: tempScore };
        TeamService.updateTeamScore(this.state.gameScores[index].teamID, teamScore)
        .then(()=>{
            window.location.href = "http://localhost:3000/history"
        })
    }

    public insertIntoDatabase = () => {
        this.state.addedTeams1.map(this.renderGameScores1);
        this.state.addedTeams2.map(this.renderGameScores2);

        const gameEntry: IGameEntry = ({
            gameDescription: this.state.gameDescription,
            date: new Date(),
            gameType: 0,
            gameScore: this.state.gameScores,
            image: this.state.uploadedFile
        });

        GameService.insertGame(gameEntry)
            .then(() => {
                this.state.gameScores.map(this.updateGameScore)
            })
    }

    public handleAddedTeams1 = (value: string) => {
        this.setState({ addedTeam1: value })
        const array = this.state.addedTeams1;
        array.push(value);
        this.setState({ addedTeams1: array });

        const arrayTeams = this.state.teams1;
        const indexTeams = arrayTeams.indexOf(arrayTeams.find((team: ITeam) => team.id === value));
        arrayTeams.splice(indexTeams, 1)
        this.setState({ teams1: arrayTeams });
    }

    public handleAddedTeams2 = (value: string) => {
        this.setState({ addedTeam2: value })
        const array = this.state.addedTeams2;
        array.push(value);
        this.setState({ addedTeams2: array });

        const arrayTeams = this.state.teams2;
        const indexTeams = arrayTeams.indexOf(arrayTeams.find((team: ITeam) => team.id === value));
        arrayTeams.splice(indexTeams, 1)
        this.setState({ teams2: arrayTeams });
    }

    public deleteChip1 = (team: ITeam) => {

        const array = this.state.addedTeams1;
        const index = array.indexOf(array.find((item: string) => item === team.id));
        array.splice(index, 1)
        this.setState({ addedTeams1: array });

        const arrayTeams = this.state.teams1;
        arrayTeams.push(team);
        this.setState({ teams1: arrayTeams });

    }

    public deleteChip2 = (team: ITeam) => {

        const array = this.state.addedTeams2;
        const index = array.indexOf(array.find((item: string) => item === team.id));
        array.splice(index, 1)
        this.setState({ addedTeams2: array });

        const arrayTeams = this.state.teams2;
        arrayTeams.push(team);
        this.setState({ teams2: arrayTeams });
    }

    public render() {

        const renderTeams = (item: ITeam, index: number) =>
            <MenuItem key={item.id} value={item.id}>
                {`${item.name}`}
            </MenuItem>

        const renderAddedTeams = (item: string, index: number) => {
            const findValue = this.state.databaseTeams.find((teamItem: ITeam) => teamItem.id === item)
            return (
                <div>
                    {findValue.name}
                </div>
            );
        }

        const renderAddedTeams1 = (item: string, index: number) => {
            const findValue = this.state.databaseTeams.find((teamItem: ITeam) => teamItem.id === item)
            return (
                <Chip
                    key={index}
                    label={findValue.name}
                    onDelete={() => this.deleteChip1(findValue)}
                />
            );
        }

        const renderAddedTeams2 = (item: string, index: number) => {
            const findValue = this.state.databaseTeams.find((teamItem: ITeam) => teamItem.id === item)
            return (
                <Chip
                    label={findValue.name}
                    onDelete={() => this.deleteChip2(findValue)}
                />
            );
        }

        const isGameEnded = (value: number) => {
            if (value >= 10) {
                this.setState({ gameEnded: true })
            }
        }

        const setRank = (teamScore1: number, teamScore2: number) => {
            if (teamScore1 > teamScore2) {
                this.setState({ teamRank1: 1, teamRank2: 2 })
            }
            else {
                this.setState({ teamRank2: 1, teamRank1: 2 })
            }
        }

        return (
            <span className="form">
                <UserHome />
                {this.state.playerSelect ?
                    <div className="leftPadding">
                        <form>
                            <TextField
                                select
                                className="textField"
                                value={this.state.addedTeam1}
                                onChange={(event) => { this.handleAddedTeams1(event.target.value) }}
                                helperText="Team 1"
                                margin="normal"
                            >
                                {this.state.teams1.map(renderTeams)}
                            </TextField>
                            <br />
                            {this.state.addedTeams1.map(renderAddedTeams1)}
                            <br />

                            <TextField
                                select
                                className="textField"
                                value={this.state.addedTeam2}
                                onChange={(event) => { this.handleAddedTeams2(event.target.value) }}
                                helperText="Team 2"
                                margin="normal"
                            >
                                {this.state.teams2.map(renderTeams)}
                            </TextField>
                            <br />
                            {this.state.addedTeams2.map(renderAddedTeams2)}
                            <br />

                            <input
                                type="button"
                                className="button"
                                value="Play"
                                onClick={() => this.validate()}
                            />
                        </form>

                    </div>
                    :
                    <div>

                        {this.state.gameEnded ?
                            <div>
                                <div className="table">
                                    <Table style={{ maxWidth: "400px" }}>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Team 1</TableCell>
                                                <TableCell>Team 2</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                <TableCell align="right">{this.state.addedTeams1.map(renderAddedTeams)}</TableCell>
                                                <TableCell align="right">{this.state.addedTeams2.map(renderAddedTeams)}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="right">{this.state.teamScore1}</TableCell>
                                                <TableCell align="right">{this.state.teamScore2}</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </div>
                                <div className="leftPadding">
                                    <form>
                                        <TextField
                                            type="text"
                                            className="textField"
                                            value={this.state.description}
                                            onChange={(event) => { this.setState({ description: event.target.value }) }}
                                            helperText="Description"
                                            margin="normal"
                                        />
                                        <br />
                                        <input
                                            type="file"
                                            onChange={(event: any) => {
                                                const reader = new FileReader();

                                                reader.readAsDataURL(event.target.files[0]);
                                                reader.onload = () => {
                                                    this.setState({ uploadedFile: reader.result })
                                                };
                                                this.setState({
                                                    uploadedFileDisplay: URL.createObjectURL(event.target.files[0]),
                                                });
                                            }
                                            }
                                        />
                                        <br />
                                        <img src={this.state.uploadedFileDisplay} />
                                        <br />
                                        <input
                                            type="button"
                                            className="button"
                                            value="Submit game"
                                            onClick={() => this.insertIntoDatabase()}
                                        />
                                    </form>
                                </div>
                            </div>
                            :
                            <div className="table">
                                <Table style={{ maxWidth: "400px" }}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Team 1</TableCell>
                                            <TableCell>Team 2</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow>
                                            <TableCell align="right">{this.state.addedTeams1.map(renderAddedTeams)}</TableCell>
                                            <TableCell align="right">{this.state.addedTeams2.map(renderAddedTeams)}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell align="right">{this.state.teamScore1}</TableCell>
                                            <TableCell align="right">{this.state.teamScore2}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell align="right">
                                                <button onClick={() => {
                                                    this.setState({ teamScore1: this.state.teamScore1 + 1 });
                                                    isGameEnded(this.state.teamScore1 + 1)
                                                    setRank(this.state.teamScore1 + 1, this.state.teamScore2)
                                                }}>
                                                    Goal
                                        </button>
                                            </TableCell>
                                            <TableCell align="right">
                                                <button onClick={() => {
                                                    this.setState({ teamScore2: this.state.teamScore2 + 1 });
                                                    isGameEnded(this.state.teamScore2 + 1)
                                                    setRank(this.state.teamScore1, this.state.teamScore2 + 1)
                                                }}>
                                                    Goal
                                        </button>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell align="right">
                                                <button onClick={() => {
                                                    this.setState({ teamScore1: this.state.teamScore1 + 2 });
                                                    isGameEnded(this.state.teamScore1 + 2)
                                                    setRank(this.state.teamScore1 + 2, this.state.teamScore2)
                                                }}>
                                                    Tesco
                                        </button>
                                            </TableCell>
                                            <TableCell align="right">
                                                <button onClick={() => {
                                                    this.setState({ teamScore2: this.state.teamScore2 + 2 });
                                                    isGameEnded(this.state.teamScore2 + 2)
                                                    setRank(this.state.teamScore1, this.state.teamScore2 + 2)
                                                }}>
                                                    Tesco
                                        </button>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </div>
                        }
                    </div>
                }
                <AiBot />    
            </span>

        );
    }
}