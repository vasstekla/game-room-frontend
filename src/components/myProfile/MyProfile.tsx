import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './MyProfile.css';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import { IUser } from '../../models/IUser';
import UserService from '../../services/UserService';
import TeamService from '../../services/TeamService';
import AiBot from '../aiBot/aiBot';

export default class MyProfile extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            userId: '',
            token: '',
            firstName: '',
            lasttName: '',
            username: '',
            emailAdress: '',
            currentEmailAdress: '',
            currentPassword: '',
            newPassword: '',
            newPasswordAgain: '',
            currentUsername: '',
            uploadedFileDisplay: require('../../img/avatar.png'),
        }
    }

    public componentWillMount() {
        if (typeof window !== 'undefined') {
            const tempUserId = localStorage.getItem('userId')
            const tempToken = localStorage.getItem('token')
            if (tempUserId !== null) {
                this.setState({ userId: tempUserId })
                this.setState({ token: tempToken })
                UserService.getUserById(tempUserId)
                    .then((dbUser: IUser) => {
                        if (dbUser.profilePicture !== '') {
                            this.setState({ uploadedFileDisplay: dbUser.profilePicture })
                        }
                        this.setState({
                            firstName: dbUser.firstName,
                            lastName: dbUser.lastName,
                            username: dbUser.username,
                            currentUsername: dbUser.username,
                            emailAdress: dbUser.emailAdress,
                            currentEmailAdress: dbUser.emailAdress,
                            uploadedFile: dbUser.profilePicture
                        })
                    })
            }
        }

    }

    public validate = () => {
        if (this.state.currentPassword === '') {
            window.alert('Please add your current password in order to save changes.')
        }
        else {
            UserService.authentificate({
                username: this.state.currentUsername,
                password: this.state.currentPassword,
            }).then((dbUser: IUser) => {
                if (dbUser.token === undefined) {
                    window.alert('Incorrect current password, please try again.')
                }
                else {
                    let valid = true
                    if (!/^[A-Z][a-zA-Z ]+$/.test(this.state.firstName) || !/^[A-Z][a-zA-Z ]+$/.test(this.state.lastName)) {
                        window.alert("The first and last names have to start with uppercase, and they can contain only letters.")
                        valid = false
                    }

                    if (!/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(this.state.username)) {
                        window.alert("The username can contain only letters, numbers and spaces.")
                        valid = false
                    }


                    if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(((accesa+\.)+[a-zA-Z]{2,}))$/.test(this.state.emailAdress)) {
                        window.alert("The email adress has to be in email format, and also from the company Accesa.")
                        valid = false
                    }


                    let tempPassword = '';
                    if (this.state.newPassword !== '' || this.state.newPasswordAgain !== '') {
                        if (this.state.newPassword !== this.state.newPasswordAgain || !/^.{6,}$/.test(this.state.newPassword)) {
                            valid = false;
                            window.alert("The passwords has to be at least 6 characters long, and the two passwords have to match.")
                        } else {
                            tempPassword = this.state.newPassword;
                        }
                    }

                    UserService.getAllUsers()
                        .then((dbUsers: IUser[]) => {
                            let emailfound;
                            let usernamefound;

                            if (this.state.currentEmailAdress !== this.state.emailAdress) {
                                emailfound = dbUsers.find((element: IUser) => {
                                    return element.emailAdress === this.state.emailAdress;
                                });
                            }

                            if (this.state.currentUsername !== this.state.username) {
                                usernamefound = dbUsers.find((element: IUser) => {
                                    return element.username === this.state.username;
                                });
                            }

                            if (!usernamefound) {
                                if (!emailfound) {
                                    if (valid) {
                                        const data: IUser = {
                                            id: this.state.userId,
                                            username: this.state.username,
                                            firstName: this.state.firstName,
                                            lastName: this.state.lastName,
                                            emailAdress: this.state.emailAdress,
                                            password: tempPassword,
                                            token: this.state.token,
                                            profilePicture: this.state.uploadedFile,
                                        };

                                        console.log(data)
                                        UserService.updateUser(this.state.userId, data)
                                            .then(() => {
                                                TeamService.updateTeamName(this.state.currentUsername, this.state.username)
                                                    .then(() => {
                                                        window.alert("Profile updated")
                                                    })
                                            })
                                    }
                                } else {
                                    window.alert("This email adress already exists in out database.")
                                }
                            } else {
                                window.alert("This username is already taken, please choose another one.")
                            }
                        })
                }
            })
        }
    }




    public render() {

        return (
            <span>
                <UserHome />
                <div className="myProfile">
                    <div className="avatar">
                        <form>
                            <input
                                type="file"
                                id="img"
                                style={{ display: "none" }}
                                onChange={(event: any) => {
                                    const reader = new FileReader();

                                    reader.readAsDataURL(event.target.files[0]);
                                    reader.onload = () => {
                                        this.setState({ uploadedFile: reader.result })
                                    };
                                    this.setState({
                                        uploadedFileDisplay: URL.createObjectURL(event.target.files[0]),
                                    });

                                }
                                } />
                            <label htmlFor="img">
                                <figure>
                                    <Avatar className="avatar" style={{ width: 100, height: 100 }} src={this.state.uploadedFileDisplay} />
                                    <figcaption>Change picture</figcaption>
                                </figure>
                            </label>
                            <br />
                            <TextField
                                type="text"
                                value={this.state.firstName}
                                onChange={(event) => { this.setState({ firstName: event.target.value }) }}
                                helperText="First Name"
                                margin="normal"
                            />
                            <br />
                            <TextField
                                type="text"
                                value={this.state.lastName}
                                onChange={(event) => { this.setState({ lastName: event.target.value }) }}
                                helperText="Last Name"
                                margin="normal"
                            />
                            <br />
                            <TextField
                                type="text"
                                value={this.state.username}
                                onChange={(event) => { this.setState({ username: event.target.value }) }}
                                helperText="Username"
                                margin="normal"
                            />
                            <br />
                            <TextField
                                type="text"
                                value={this.state.emailAdress}
                                onChange={(event) => { this.setState({ emailAdress: event.target.value }) }}
                                helperText="emailAdress"
                                margin="normal"
                            />
                            <br />
                            <TextField
                                type="password"
                                value={this.state.currentPassword}
                                onChange={(event) => { this.setState({ currentPassword: event.target.value }) }}
                                helperText="Current password"
                                margin="normal"
                            />
                            <br />
                            <TextField
                                type="password"
                                value={this.state.newPassword}
                                onChange={(event) => { this.setState({ newPassword: event.target.value }) }}
                                helperText="New password"
                                margin="normal"
                            />
                            <br />
                            <TextField
                                type="password"
                                value={this.state.newPasswordAgain}
                                onChange={(event) => { this.setState({ newPasswordAgain: event.target.value }) }}
                                helperText="New password again"
                                margin="normal"
                            />
                            <br />
                            <input
                                type="button"
                                className="button"
                                value="Save changes"
                                onClick={() => this.validate()}
                            />
                        </form>
                    </div>
                </div>
                <AiBot />
            </span>

        );
    }
}