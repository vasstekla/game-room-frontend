import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ImageSlider from './components/imageSlider/ImageSlider';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ImageSlider />, div);
  ReactDOM.unmountComponentAtNode(div);
});
