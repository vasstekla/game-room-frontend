import axios, { AxiosResponse } from 'axios'
import { config } from '../utils/config';
import { IUser } from '../models/IUser';
import { IUserAuthentificate } from '../models/IUserAuthentificate';

export default class TeamService {

    public static insertUser = (user: IUser)  => {
        return axios.post(`${config.apiUrl}/User`,user)
        .then((result: AxiosResponse) => result.data)
    }

    public static getAllUsers = (): Promise<IUser[]> => {
        return axios.get(`${config.apiUrl}/users`)
            .then((result: AxiosResponse) => result.data)
    }

    public static getUserById = (id: string): Promise<IUser> => {
        return axios.get(`${config.apiUrl}/user?id=${id}`)
            .then((result: AxiosResponse) => result.data)
    }

    public static updateUser = (userId: string, user: IUser) => {
        return axios.put(`${config.apiUrl}/user/${userId}`, user)
            .then((result: AxiosResponse) => result.data)
    }

    public static authentificate = (user: IUserAuthentificate): Promise<IUser> => {
        return axios.post(`${config.apiUrl}/users/login`, user)
            .then((result: AxiosResponse) => result.data)
    }

}