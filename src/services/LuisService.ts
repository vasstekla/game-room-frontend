import axios, { AxiosResponse } from 'axios'
import { config } from '../utils/config';
import { ILuisData } from '../models/ILuisData';

export default class LuisService {

    public static send = (query: string): Promise<ILuisData> => {
        return axios.get(`${config.luisUrl}${query}`)
            .then((result: AxiosResponse) => result.data)
    }
}