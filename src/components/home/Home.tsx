import * as React from 'react';
import ToolBar from '../toolbar/Toolbar';
import ImageSlider from '../imageSlider/ImageSlider';
import SignUp from '../signUp/SignUp';
import MyProfile from '../myProfile/MyProfile';

export default class Home extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
    }

    public render() {
        if (typeof window !== 'undefined'){
            if (localStorage.getItem('token') == null) {
                return (
                    <div>
                        <ToolBar loggedin={false} />
                        <ImageSlider />
                        <SignUp />
                    </div>
                );
            }
            else {
                window.location.href = "http://localhost:3000/profile"
                return (
                    <MyProfile />
                );
            }
        }        
    }
}