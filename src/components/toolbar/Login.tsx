import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import './Login.css';
import UserService from '../../services/UserService'
import { IUser } from '../../models/IUser'

export default class Login extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            username: '',
            password: '',

            user: ''
        }
    }

    public render() {
        return (
            <Grid container spacing={16}>
                <Grid item>
                    <form>
                        <Grid container spacing={8}>
                            <Grid item>
                                <label>
                                    Username:
                                <br />
                                    <input
                                        type="text"
                                        name="name"
                                        value={this.state.username}
                                        onChange={(event) => this.setState({ username: event.target.value })} />
                                </label>
                            </Grid>
                            <Grid item>
                                <label>
                                    Password:
                                <br />
                                    <input
                                        type="password"
                                        name="password"
                                        value={this.state.password}
                                        onChange={(event) => this.setState({ password: event.target.value })} />
                                </label>
                            </Grid>
                            <Grid item>
                                <br />

                                <input
                                    type="button"
                                    className="button"
                                    value="Log in"
                                    onClick={(event) => this.validate()} />

                            </Grid>
                        </Grid>
                    </form>
                </Grid>
                <Grid item>
                    <form action="#signup">
                        <Grid container spacing={8}>
                            <Grid item>
                                <br />
                                <input className="button" type="submit" value="Sign up" />
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        );
    }

    public validate() {
        UserService.authentificate({
            username: this.state.username,
            password: this.state.password,
        }).then((dbUser: IUser) => {
            if (dbUser.token === undefined) {
                window.alert('Incorrect current password, please try again.')
            } else {
                this.setState({ user: dbUser })
                if (typeof window !== 'undefined') {
                    localStorage.setItem('token', dbUser.token)
                    if (dbUser.id !== undefined) {
                        localStorage.setItem('userId', dbUser.id)
                    }
                    window.location.href = "http://localhost:3000/profile"
                }

            }
        })

    }
}